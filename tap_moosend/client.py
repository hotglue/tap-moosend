"""REST client handling, including MooSendStream base class."""

import requests
from typing import Any, Dict, Optional, Union, List, Iterable
from pendulum import from_timestamp

from memoization import cached

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import APIKeyAuthenticator


class MooSendStream(RESTStream):
    """MooSend stream class."""

    url_base = "https://api.moosend.com/v3"

    next_page_token_jsonpath = "$.Context.Paging[*]"

    use_url_pagination = False
    page = 1

    @property
    def authenticator(self) -> APIKeyAuthenticator:
        """Return a new authenticator object."""
        return APIKeyAuthenticator.create_for_stream(
            self, key="apikey", value=self.config.get("api_key"), location="params"
        )

    @property
    def path(self):
        page_size = self.config.get("page_size", 1000)
        current_page = self.page
        return f"{self.original_path}/{current_page}/{page_size}.json"

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        all_matches = extract_jsonpath(self.next_page_token_jsonpath, response.json())
        first_match = next(iter(all_matches), None)

        if first_match == None:
            return None

        if first_match.get("TotalResults") == 0:
            return None

        current_page = first_match["CurrentPage"]
        total_pages = first_match["TotalPageCount"]

        if current_page < total_pages:
            self.page = current_page + 1
            return current_page + 1

        return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        if self.use_url_pagination:
            return {}

        params: dict = {}
        if next_page_token:
            params["Page"] = next_page_token
        return params

    def post_process(self, row: dict, context: Optional[dict] = None) -> Optional[dict]:

        if self.replication_key in row.keys() and row[self.replication_key] is not None:
            time = (
                int("".join([n for n in row[self.replication_key] if n.isdigit()]))
                / 1000
            )

            if self.name == "campaigns" or self.name == "subscribers":
                time = time / 10000

            if time > 4124116939:
                if "CreatedOn" in row.keys():
                    time = (
                        int("".join([n for n in row["CreatedOn"] if n.isdigit()]))
                        / 1000
                    )
                else:
                    time = 0

            row[self.replication_key] = from_timestamp(time)
        return row
