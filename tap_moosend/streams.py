"""Stream type classes for tap-moosend."""

from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th

from tap_moosend.client import MooSendStream


class MailingListsStream(MooSendStream):
    """Define custom stream."""

    name = "mailing_lists"
    # Uses URL pagination
    original_path = "/lists"
    records_jsonpath = "$.Context.MailingLists[*]"
    primary_keys = ["ID"]
    use_url_pagination = True
    # replication_key = "UpdatedOn"
    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("ActiveMemberCount", th.IntegerType),
        th.Property("BouncedMemberCount", th.IntegerType),
        th.Property("RemovedMemberCount", th.IntegerType),
        th.Property("UnsubscribedMemberCount", th.IntegerType),
        th.Property("Status", th.IntegerType),
        th.Property(
            "CustomFieldsDefinition", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("CreatedBy", th.StringType),
        th.Property("CreatedOn", th.DateTimeType),
        th.Property("UpdatedBy", th.StringType),
        th.Property("UpdatedOn", th.DateTimeType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"list_id": record["ID"]}


class SubscribersStream(MooSendStream):
    """Define custom stream."""

    name = "subscribers"
    # Uses standard pagination
    path = "/lists/{list_id}/subscribers.json"
    records_jsonpath = "$.Context.Subscribers[*]"
    primary_keys = ["ID"]
    # replication_key = "UpdatedOn"
    parent_stream_type = MailingListsStream
    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("Email", th.StringType),
        th.Property("CreatedOn", th.DateTimeType),
        th.Property("UpdatedOn", th.DateTimeType),
        th.Property("UnsubscribedOn", th.DateTimeType),
        th.Property("UnsubscribedFromID", th.CustomType({"type": ["number", "string"]})),
        th.Property("SubscribeType", th.IntegerType),
        th.Property("SubscribeMethod", th.IntegerType),
        th.Property(
            "CustomFieldsDefinition", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("RemovedOn", th.DateTimeType),
        th.Property("Tags", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()

    def parse_response(self, response):
        return super().parse_response(response)


class CampaignsStream(MooSendStream):
    """Define custom stream."""

    name = "campaigns"
    # Uses URL pagination
    original_path = "/campaigns"
    records_jsonpath = "$.Context.Campaigns[*]"
    use_url_pagination = True
    primary_keys = ["ID"]

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("Subject", th.StringType),
        th.Property("SiteName", th.StringType),
        th.Property("ConfirmationTo", th.StringType),
        th.Property("CreatedOn", th.DateTimeType),
        th.Property("ABHoursToTest", th.StringType),
        th.Property("ABCampaignType", th.StringType),
        th.Property("ABWinner", th.StringType),
        th.Property("ABWinnerSelectionType", th.StringType),
        th.Property("Status", th.IntegerType),
        th.Property("DeliveredOn", th.DateType),
        th.Property("ScheduledFor", th.DateType),
        th.Property("ScheduledForTimezone", th.StringType),
        th.Property(
            "MailingLists",
            th.ArrayType(
                th.ObjectType(
                    th.Property("Campaign", th.StringType),
                    th.Property(
                        "MailingList",
                        th.ObjectType(
                            th.Property("ID", th.StringType),
                            th.Property("Name", th.StringType),
                            th.Property("Email", th.StringType),
                            th.Property("CreatedOn", th.DateTimeType),
                            th.Property("UpdatedOn", th.DateTimeType),
                            th.Property("UnsubscribedOn", th.DateTimeType),
                            th.Property("UnsubscribedFromID", th.IntegerType),
                            th.Property("SubscribeType", th.IntegerType),
                            th.Property("SubscribeMethod", th.IntegerType),
                            th.Property(
                                "CustomFieldsDefinition",
                                th.CustomType({"type": ["array", "string"]}),
                            ),
                            th.Property("RemovedOn", th.DateTimeType),
                            th.Property(
                                "Tags", th.CustomType({"type": ["array", "string"]})
                            ),
                        ),
                    ),
                    th.Property(
                        "Segment", th.CustomType({"type": ["object", "string"]})
                    ),
                )
            ),
        ),
        th.Property("TotalSent", th.IntegerType),
        th.Property("TotalOpens", th.IntegerType),
        th.Property("UniqueOpens", th.IntegerType),
        th.Property("TotalBounces", th.IntegerType),
        th.Property("TotalForwards", th.IntegerType),
        th.Property("UniqueForwards", th.IntegerType),
        th.Property("TotalLinkClicks", th.IntegerType),
        th.Property("UniqueLinkClicks", th.IntegerType),
        th.Property("RecipientsCount", th.IntegerType),
        th.Property("IsTransactional", th.BooleanType),
        th.Property("TotalComplaints", th.IntegerType),
        th.Property("TotalUnsubscribes", th.IntegerType),
        th.Property("CampaignSource", th.BooleanType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"campaign_id": record["ID"]}


class BouncedEmailsStream(MooSendStream):
    """Define custom stream."""

    name = "bounced_emails"
    # Uses standard pagination
    path = "/campaigns/{campaign_id}/stats/bounced.json"
    records_jsonpath = "$.Context.Analytics[*]"
    primary_keys = None
    # replication_key = "CreatedOn"
    parent_stream_type = CampaignsStream
    schema = th.PropertiesList(
        th.Property("Context", th.StringType),
        th.Property("ContextName", th.StringType),
        th.Property("TotalCount", th.NumberType),
        th.Property("UniqueCount", th.NumberType),
        th.Property("ContextDescription", th.StringType),
    ).to_dict()


class UnsubscribedEmailsStream(MooSendStream):
    """Define custom stream."""

    name = "unsubscribed_emails"
    # Uses standard pagination
    path = "/campaigns/{campaign_id}/stats/unsubscribed.json"
    records_jsonpath = "$.Context.Analytics[*]"
    primary_keys = None
    # replication_key = "CreatedOn"
    parent_stream_type = CampaignsStream
    schema = th.PropertiesList(
        th.Property("Context", th.StringType),
        th.Property("ContextName", th.StringType),
        th.Property("TotalCount", th.NumberType),
        th.Property("UniqueCount", th.NumberType),
        th.Property("ContextDescription", th.StringType),
    ).to_dict()
